class Comment < ActiveRecord::Base
  belongs_to :post #each comment belongs to a single post
  validates_presence_of :post_id #check if the post id was provided
  validates_presence_of :body   #body should be provided

end
