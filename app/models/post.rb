class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy #a post can be associated with many comments,also delete all  associated
                                        # comments when a post is deleted
  validates_presence_of :title
  validates_presence_of :body

end
